<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','UserController@search')->name('search');

Route::group(['prefix' => 'admin','middleware'=>'adminlogin'],function() {
  Route::get('changeUserPass/{idUser}','AdminController@getChangeUserPass')->name('changeUserPass');;
  Route::post('changeUserPass/{idUser}','AdminController@postChangeUserPass');
  Route::get('search','AdminController@search')->name('searchAdmin');
  Route::get('deletePaper/{idPaper}','AdminController@deletePaper')->name('deletePaper');
  Route::get('addData','AdminController@getAddData')->name('add_data');
  Route::post('addData','AdminController@postAddData');
	Route::get('logout','AdminController@logout')->name('logout');

});
Route::get('admin','UserController@getLogin')->name('login');
Route::get('login','UserController@getLogin')->name('login');

Route::post('login','UserController@postLogin');
Route::get('list','UserController@getList')->name('list');
Route::get('search','UserController@search')->name('search');
