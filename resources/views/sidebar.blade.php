@if((Auth::user()))
  <nav class="navbar navbar-default background-nav" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="{{route('add_data')}}">Add data</a>
        <a class="navbar-brand" href="{{route('searchAdmin')}}">Delete papers</a>
      </div>

      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{Auth::user()->username}}<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="{{route('changeUserPass',['id'=> Auth::user()->id])}}">Change user name and password</a></li>
              <div class="divider"></div>
              <li><a href="{{route('logout')}}">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>
@else
<nav class="navbar navbar-default background-nav" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="{{route('list')}}">List</a>
        <a class="navbar-brand" href="{{route('search')}}">Search papers</a>
      </div>

      <div class="collapse navbar-collapse navbar-ex1-collapse">
      </div>
    </div>
  </nav>

@endif
