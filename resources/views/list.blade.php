<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List</title>
    <base href="{{asset('')}}">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/style_list.css">

</head>
<body>
    @include('sidebar')
    <div class="page-header text-center">
        <h2>LIST</h2>
    </div>
        <div class="container">
            <div class="row">
                <form action="/list" method="get" id="form">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <select class="form-control" id="sel1" name = "att1">
                                <option value="paper">Papers</option>
                                <option value="author">Authors</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-right">
                        <div class="form-group">
                            <label>From:</label>
                        </div>
                    </div>

                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="form-group">
                            <select class="form-control" id="sel2" name = "att2">
                                <option value="author">Authors</option>
                                <option value="institute">Institute</option>
                                <option value="city">City/State</option>
                                <option value="country">Country</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <input type="text" class="form-control" id="search" name="keyword">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-default">OK</button>
                </form>
            </div>
            @if($paper)
                @if(count($paper)>0)
                    <div class="container">
                        @foreach($paper as $row)
                            <div class="ket-qua-1">
                                <h3 class="title"><a href="{{$row['uurl']}}" target="_blank">{{$row['title']}}</a></h3>
                                <p class="author">
                                            @foreach($row['authors'] as $au)
                                            <a href=" {{$au['aurl']}}" target="_blank">{{$au['surname']}} {{$au['givenName']}}</a>,
                                            @endforeach
                                        </p>
                                <div>
                                    <span class="issn">{{$row['issn']}}</span>
                                    <span class="cover">{{$row['coverDate']}}</span>
                                </div>
                                <div class="content text-justify">
                                {!!$row['abstract']!!}
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    </div>

                <center>
    		        {{$paper->links()}}
    	        </center>
                @else
                    <h3>Không có kết quả!</h3>
                @endif
            @else
                @if($author)
                    @if(count($author)>0)
                        <div class="container">
                          <div class="row">
                            @foreach($author as $row)
                                <div class="col-md-4">
                                     <div class="ket-qua-1" style="border: 2px solid #0000ff;border-radius: 12px;padding:2px;">
                                        <h3 class="title">
                                            <a href="{{$row->aurl}}">{{$row->surname}} {{$row->givenName}}</a>
                                        </h3>
                                        <p class="author">{{$row->institute}}</p>
                                        <div>
                                            <span class="issn">{{$row->city}}</span>
                                        </div>
                                        <div>
                                            <span class="cover">{{$row->country}}</span>
                                        </div>
                                        <div class="content text-justify">
                                        {{-- {!!$row->abstract!!} --}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                          </div>
                        </div>
                    <center>
                        {{$author->links()}}
                    </center>
                    @else
                        <h3>Không có kết quả!</h3>
                    @endif
                @endif
            @endif
    </div>
</body>
</html>

<script type="text/javascript">
    $("#sel1").change(function(){
        if($("#sel1").val() == "author"){
            $("#sel2 option[value='author']").each(function() {
                $(this).remove();
            });
        } else {
            $("#sel2").html("<option value='author'>Authors</option> <option value='institute'>Institute</option> <option value='city'>City/State</option> <option value='country'>Country</option>")
        }

    });
    $("#form").submit(function (e) {
        if($("#search").val()==""){
            e.preventDefault();
            alert("Bạn phải điền từ khóa!");
        }
    });
</script>
