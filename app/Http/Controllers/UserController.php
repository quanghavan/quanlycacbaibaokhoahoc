<?php

namespace App\Http\Controllers;
use DB;
use App\PaperManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //hiển thị trang login
    public function getLogin(){
        if(!Auth::user()){
            return view('login');
        } else {
            return redirect()->back();
        }
    }
    //xử lý việc login
    public function postLogin(Request $request){

        $this->validate($request,[
            'username' => 'required',
            'password' => 'required'
        ],[
            'username.required' => 'Vui lòng nhập vào username',
            'password.required' => 'Vui lòng nhập vào password'
        ]);
        if(Auth::attempt(['username' =>$request->username,'password' => $request->password])){
            return redirect()->intended('/admin/search');
        }
        return redirect('login')->with('thongbao','Đăng nhập không thành công');

    }
    //hiển thị trang danhsach
    public function getList(Request $request){
        switch($request->att1){
            case "paper":
                $pm = new PaperManager();
                $paper = $pm->getListPapers($request->att2, $request->keyword);
                $paper->setPath('list?att1='.$request->att1.'&att2='.$request->att2.'&keyword='.$request->keyword);
                return view('list',['paper' => $paper, 'author' => null]);
            case "author":
                $pm = new PaperManager();
                $author = $pm->getListAuthors($request->att2, $request->keyword);
                $author->setPath('list?att1='.$request->att1.'&att2='.$request->att2.'&keyword='.$request->keyword);
                return view('list',['author' => $author, 'paper' => null]);
            default:
                return view('list',['paper' => null, 'author' => null]);
        }
    }
    //hiển thị trang tìm kiếm và xử lý việc tìm kiếm
    public function search(Request $request){
		if($request->fulltext_search=='false'){
            if($request->author != ""){
                $str = preg_split ('/[\s,]+/',$request->author);
    			$arr[] = array('author.surname','like',$str[0].'%');
    			$givenName[] = array('author.givenName','like',$str[0].'%');
    			if(count($str)>1){
    				$arr[] = array('author.givenName','like',$str[1].'%');
    			}
            }
            if($request->keywords != ""){
              $arr[] = array('paper.keywords','like',$request->keywords.'%');
            }
            if($request->institute != ""){
              $arr[] = array('institute.name','like',$request->institute.'%');
            }
            if($request->city_state != ""){
              $arr[] = array('city.name','like',$request->city_state.'%');
            }
            if($request->country != ""){
              $arr[] = array('country.name','like',$request->country.'%');
            }
            if(isset($request->author)||isset($request->keywords)
              ||isset($request->institute)||isset($request->city_state)
              ||isset($request->country)){
              $pm = new PaperManager();
    		  if(!isset($givenName)){
    			$givenName[] = array('author.givenName','=','.');
    		  }
              $paper = $pm->searchByField($arr,$givenName);
              $paper->setPath('search?word='.$request->word.'&author='.$request->author.'&keywords='.$request->keywords.'&institute='.$request->institute
                    .'&city_state='.$request->city_state.'&country='.$request->country.'&fulltext_search='.$request->fulltext_search);
              return view('search_paper',['paper' => $paper]);
            }
            else{
                return view('search_paper');
            }
		}else{
            if(isset($request->word)){
                $pm = new PaperManager();
                $paper = $pm->searchByPhrase($request->word);
                $paper->setPath('search?word='.$request->word.'&author='.$request->author.'&keywords='.$request->keywords.'&institute='.$request->institute
                    .'&city_state='.$request->city_state.'&country='.$request->country.'&fulltext_search='.$request->fulltext_search);
                return view('search_paper',['paper' => $paper]);
            }else{
                return view('search_paper');
            }
        }
    }
}
