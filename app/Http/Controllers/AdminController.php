<?php

namespace App\Http\Controllers;

use App\AuthenticationManager;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PaperManager;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    //đăng xuất
    public function logout(){
        Auth::logout();
        return redirect('login');
    }
    //hiển thị trang thay đổi mật khẩu
    public function getChangeUserPass($idUser){
        return view('change_password');
    }
    //xử lý việc thay đổi mật khẩu
    public function postChangeUserPass(Request $request,$idUser){
        $this->validate($request,[
            'username' => 'required|min:4',
            'password' => 'required|min:6',
            'passwordAgain' => 'required|same:password'
        ],[
            'username.required' => 'Bạn chưa điền username',
            'username.min' => 'Username phải chứa ít nhất 4 kí tự',
            'password.required' => 'Bạn chưa điền mật khẩu',
            'password.min' => 'Mật khẩu phải chứa ít nhất 6 kí tự',
            'passwordAgain.required' => 'Bạn chưa nhập lại mật khẩu',
            'passwordAgain.same' => 'Mật khẩu không khớp',
        ]);
        $user = new User();
        $user->id = $idUser;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $au = new AuthenticationManager();
        $au->changeUserPass($user);
        return redirect('admin/changeUserPass/'.$user->id)->with('thongbao','Thay đổi tài khoản thành công');
    }
    //xóa bài báo
    public function deletePaper($idPager,Request $request){
        $pm = new PaperManager();
        $pm->deletePaper($idPager);
        return redirect('/admin/search')->with('thongbao','Xóa thành công');
    }
    //tìm kiếm bài báo
    public function search(Request $request){
        if($request->fulltext_search=='false'){
            if($request->author != ""){
                $str = preg_split ('/[\s,]+/',$request->author);
                $arr[] = array('author.surname','like',$str[0].'%');
                $givenName[] = array('author.givenName','like',$str[0].'%');
                if(count($str)>1){
                    $arr[] = array('author.givenName','like',$str[1].'%');
                }
            }
            if($request->keywords != ""){
                $arr[] = array('paper.keywords','like',$request->keywords.'%');
            }
            if($request->institute != ""){
                $arr[] = array('institute.name','like',$request->institute.'%');
            }
            if($request->city_state != ""){
                $arr[] = array('city.name','like',$request->city_state.'%');
            }
            if($request->country != ""){
                $arr[] = array('country.name','like',$request->country.'%');
            }
            $pm = new PaperManager();
            if(isset($request->author)||isset($request->keywords)
                ||isset($request->institute)||isset($request->city_state)
                ||isset($request->country)){
                if(!isset($givenName)){
                    $givenName[] = array('author.givenName','=','.');
                }
                $paper = $pm->searchByField($arr,$givenName);
                $paper->setPath('admin/search?word='.$request->word.'&author='.$request->author.'&keywords='.$request->keywords.'&institute='.$request->institute
                    .'&city_state='.$request->city_state.'&country='.$request->country.'&fulltext_search='.$request->fulltext_search);
            }
            else{
                $paper = $pm->listPaper();
            }
            return view('remove_paper',['paper' => $paper]);
        }else{
            $pm = new PaperManager();
            if(isset($request->word)){
                $paper = $pm->searchByPhrase($request->word);
            }else{
                $paper = $pm->listPaper();
                $paper->setPath('/admin/search');
            }
            return view('remove_paper',['paper' => $paper]);
        }
    }
    //hiển thị trang thêm bài báo
    public function getAddData(Request $request){

        if($request->has('success_message') || $request->has('error_message')){
            return view('add_data', ['success_message'=>$request->input('success_message'),
                'error_message'=>$request->input('error_message')]);
        }
        return view('add_data', ['success_message'=>"", 'error_message'=>'']);
    }
    //xử lý việc thêm bài báo
    public function postAddData(Request $request){
        $paper_manager = new PaperManager();
        return $paper_manager->addData($request);
    }
}
